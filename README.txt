One2All, the ActiveCampaign Email Marketing Newsletter Signup Box

This module implements ActiveCampaign Email Marketing software APIs.

* It's main purpose is to display configurable newsletter signup block.
* Email addresses submitted by website users are stored in ActiveCampaign software database via API calls.
* This module was created with Domain Access module in mind and it allows setting different subscription lists per domain
* All configuration for this module is done in block configuration section. (There is no designated admin area, all configuration done per block)
* Code detects if Domain Access (domain) module is enabled and creates different configuration variables per domain

Description of software that this module connects to is available at: http://www.activecampaign.com/emailmarketing/